export default (ms, fn) => {
  let timerId;

  return (...args) => {
    clearTimeout(timerId);

    timerId = setTimeout(() => fn(...args), ms);
  }
}
