import React, { PureComponent } from 'react';
import pt from 'prop-types';
import styles from './Converter.module.css';
import numberInputHOC from '../numberInputHOC';
import { Col, Input, Row, Typography } from 'antd';

const { Text } = Typography;

const NumberInput = numberInputHOC(Input);

const round = value => Number(value.toFixed(2));

export default class Converter extends PureComponent {
    static propTypes = {
      rate: pt.number,
      value: pt.number,
      size: pt.string,
      prefixFrom: pt.string,
      placeholderFrom: pt.string,
      prefixTo: pt.string,
      placeholderTo: pt.string,
      onChange: pt.func,
      errorMessage: pt.string,
    };

    state = {};

    handleToValueChange = value => {
      const { onChange, rate } = this.props;
      const valueFrom = round(value / rate);

      return onChange(value !== undefined ? valueFrom : undefined);
    };

    render() {
        const { value, size, placeholderFrom, placeholderTo, prefixFrom, prefixTo, rate, onChange, errorMessage } = this.props;
        const valueTo = round(value * rate);

        return (
          <div className={styles.container}>
            <Row>
              <Col span={12}>
                <NumberInput
                  className={errorMessage && styles.errorInput}
                  value={value}
                  size={size}
                  placeholder={placeholderFrom}
                  prefix={prefixFrom}
                  onChange={onChange}
                />
              </Col>
              <Col span={12}>
                <div className={styles.inputTo}>
                  <NumberInput
                    value={value !== undefined ? valueTo : undefined}
                    size={size}
                    placeholder={placeholderTo}
                    prefix={prefixTo}
                    onChange={this.handleToValueChange}
                  />
                </div>
              </Col>
            </Row>
            <div className={styles.errorMessage}>
              <Text type="danger">{errorMessage}</Text>
            </div>
          </div>
        );
    }
}
