import React from 'react';
import Converter from '../Converter';
import { mount, shallow } from 'enzyme';

describe('Converter', () => {
  it('should calculate right value when enter left value', () => {
    const component = shallow(<Converter value={650} rate={0.15} onChange={() => {}} />);

    expect(component.find('numberInputHOC(Input)').at(1).prop('value')).toBe(97.5);

    component.setProps({ value: 1300 });

    expect(component.find('numberInputHOC(Input)').at(1).prop('value')).toBe(195);
  });

  it('should calculate left value when enter right value', () => {
    let value;
    const component = mount(<Converter value={value} rate={0.15} onChange={(v) => value = v} />);

    component.find('input').at(1).simulate('change', { target: { value: '150' } });

    expect(value).toBe(1000);

    component.find('input').at(1).simulate('change', { target: { value: '300' } });

    expect(value).toBe(2000);
  });

  it('clean right value when cleaning left value', () => {
    let value = 650;
    const component = mount(<Converter value={value} rate={0.15} onChange={(v) => value = v} />);

    component.find('input').at(0).simulate('change', { target: { value: '' } });
    component.setProps({ value });
    expect(component.find('input').at(0).prop('value')).toBe('');
    expect(component.find('input').at(1).prop('value')).toBe('');
  });

  it('clean left value on right value cleaning', () => {
    let value = 650;
    const component = mount(<Converter value={value} rate={0.15} onChange={(v) => value = v} />);

    component.find('input').at(1).simulate('change', { target: { value: '' } });
    component.setProps({ value });
    expect(component.find('input').at(0).prop('value')).toBe('');
    expect(component.find('input').at(1).prop('value')).toBe('');
  });

  it('should calculate right value when exchange rate is changed', () => {
    const component = shallow(<Converter value={650} rate={0.15} onChange={() => {}} />);

    expect(component.find('numberInputHOC(Input)').at(1).prop('value')).toBe(97.5);

    component.setProps({ rate: 0.16 });

    expect(component.find('numberInputHOC(Input)').at(1).prop('value')).toBe(104);
  });
});
