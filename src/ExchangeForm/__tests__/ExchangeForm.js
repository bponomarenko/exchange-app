import React from 'react';
import ExchangeForm from '../ExchangeForm';
import { mount, shallow } from 'enzyme';

const requestFn = (currencyFrom, currencyTo, amount) => Promise.resolve({
  currencyFrom,
  currencyTo,
  rate: 1.25,
  amount: amount * 1.25
});

describe('ExchangeCode', () => {
  it('make exchange request on mount', () => {
    const request = jest.fn(requestFn);
    shallow(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);

    expect(request).toBeCalledTimes(1);
  });

  it('make exchange request on left account changing', () => {
    const request = jest.fn(requestFn);
    const component = mount(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);

    component.find('Select').at(0).prop('onChange')('RevolutGBP');

    expect(request).toBeCalledTimes(2);
  });

  it('make exchange request on right account changing', () => {
    const request = jest.fn(requestFn);
    const component = mount(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);

    component.find('Select').at(1).prop('onChange')('RevolutGBP');

    expect(request).toBeCalledTimes(2);
  });

  it('swap accounts on selecting the same accounts', () => {
    const request = jest.fn(requestFn);
    const component = mount(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);

    component.find('Select').at(0).prop('onChange')('RevolutEUR');

    expect(component.state('accountFrom').id).toBe('RevolutEUR');
    expect(component.state('accountTo').id).toBe('RevolutUSD');
  });

  it('make exchange request on input changing', () => {
    jest.useFakeTimers();
    const request = jest.fn(requestFn);
    const component = mount(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);

    component.find('Converter').prop('onChange')(500);

    jest.runOnlyPendingTimers();

    expect(request).toBeCalledTimes(2);
    expect(request.mock.calls[1]).toEqual(['USD', 'EUR', 500]);
  });

  it('show error when amount not specified', () => {
    const request = jest.fn(requestFn);
    const component = mount(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);

    component.find('Button').prop('onClick')();

    expect(component.state('errorMessage')).toBe('Enter the amount of transfer');
  });

  it('show error when not enough money on account', () => {
    const request = jest.fn(requestFn);
    const component = mount(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);

    component.setState({ amount: 10000 });
    component.find('Button').prop('onClick')();

    expect(component.state('errorMessage')).toBe('Not enough money');
  });

  it('hide errors on the user changing form state', () => {
    const request = jest.fn(requestFn);
    const component = mount(<ExchangeForm makeRequest={request} accounts={[
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]} />);
    component.setState({ errorMessage: 'Enter the amount of transfer' });
    component.find('Converter').prop('onChange')(500);

    expect(component.state('errorMessage')).toBe(undefined);

    component.setState({ errorMessage: 'Enter the amount of transfer' });
    component.find('Select').at(0).prop('onChange')('RevolutEUR');
    expect(component.state('errorMessage')).toBe(undefined);
  });
});
