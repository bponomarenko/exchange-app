import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Interval from '../Interval/Interval';
import { Typography, Select, Row, Col, Button, notification, Result } from 'antd';
import styles from './ExchangeForm.module.css';
import debounce from '../debounce';
import resolveLast from '../resolveLast';
import Converter from '../Converter/Converter';

const { Title, Text } = Typography;
const { Option } = Select;

const currencySymbols = {
  'USD': '\u0024',
  'EUR': '\u20AC',
  'GBP': '\u00A3',
};

class ExchangeForm extends PureComponent {
  static propTypes = {
    accounts: PropTypes.array,
    onExchange: PropTypes.func,
    makeRequest: PropTypes.func,
  };

  state = {
    accountFrom: this.props.accounts[0],
    accountTo: this.props.accounts[1],
  };

  componentDidMount() {
    this.updateExchangeRate();
  }

  updateExchangeRate = resolveLast(() => {
    const { makeRequest } = this.props;
    const { accountTo, accountFrom, amount = 1 } = this.state;
    const { currency: currencyFrom } = accountFrom;
    const { currency: currencyTo } = accountTo;

    return makeRequest(currencyFrom, currencyTo, amount)
      .catch(() => {
        notification.error({
          message: 'Exchange rate information is unavailable',
          duration: 5
        })
      })
      .then((rateInfo) => this.setState({ rateInfo }))
  });

  updateExchangeRateDebounced = debounce(500, this.updateExchangeRate);

  handleSelectAccountFrom = (accountFromId) => {
    const { accounts } = this.props;
    const { accountFrom, accountTo } = this.state;
    const account = accounts.find(({ id }) => id === accountFromId);

    this.setState({ accountFrom: account, errorMessage: undefined }, this.updateExchangeRate);

    if (account === accountTo) {
      this.setState({ accountTo: accountFrom })
    }
  };

  handleSelectAccountTo = (accountToId) => {
    const { accounts } = this.props;
    const { accountFrom, accountTo } = this.state;
    const account = accounts.find(({ id }) => id === accountToId);

    this.setState({ accountTo: account, errorMessage: undefined }, this.updateExchangeRate);

    if (account === accountFrom) {
      this.setState({ accountFrom: accountTo })
    }
  };

  handleInputChange = amount => this.setState({ amount, errorMessage: undefined }, this.updateExchangeRateDebounced);

  handleCloseSuccessScreen = () => this.setState({ showSuccessScreen: false });

  handleSubmit = () => {
    const { onExchange } = this.props;
    const { amount, accountFrom, rateInfo, accountTo } = this.state;

    if (!amount) {
      return this.setState({ errorMessage: 'Enter the amount of transfer' });
    }

    if (amount > accountFrom.value) {
      return this.setState({ errorMessage: 'Not enough money' });
    }

    this.setState({ loading: true });
    return this.updateExchangeRate()
      .then(() => onExchange(amount, rateInfo.rate, accountFrom, accountTo))
      .then((accounts) => {
        this.setState({ showSuccessScreen: true });

        return accounts;
      })
      .then(([accountFrom, accountTo]) => this.setState({ amount: undefined, accountFrom, accountTo }))
      .then(this.updateExchangeRate)
      .catch(() => {
        notification.error({
          message: 'Something went wrong',
          description: 'Please try again',
          duration: 5
        })
      })
      .finally(() => this.setState({ loading: false }))
  };

  render() {
    const { accounts } = this.props;
    const { accountFrom, accountTo, amount, rateInfo, loading, errorMessage, showSuccessScreen } = this.state;

    if (showSuccessScreen) {
      return (
        <Result
          status="success"
          title="Transaction completed successfully!"
          subTitle="Would you like to make another exchange?"
          extra={(
            <Button type="primary" key="make" onClick={this.handleCloseSuccessScreen}>
              Make another exchange
            </Button>
          )}
        />
      )
    }

    return (
      <Interval callback={this.updateExchangeRate} delay={10000} skipFirst>
        <Row>
          <Col span={16}>
            <Title level={2}>Exchange Form</Title>
            <Row>
              <Col span={12}>
                <div className={styles.accountFrom}>
                  <Title level={4}>From</Title>
                  <Select size="large" onChange={this.handleSelectAccountFrom} value={accountFrom.id}>
                    {accounts.map(({ id, value, currency }) => (
                      <Option value={id} key={id}>
                        {value} {currencySymbols[currency]}
                      </Option>
                    ))}
                  </Select>
                </div>
              </Col>
              <Col span={12}>
                <div className={styles.accountTo}>
                  <Title level={4}>To</Title>
                  <Select size="large" onChange={this.handleSelectAccountTo} value={accountTo.id}>
                    {accounts.map(({ id, value, currency }) => (
                      <Option value={id} key={id}>
                        {value} {currencySymbols[currency]}
                      </Option>
                    ))}
                  </Select>
                </div>
              </Col>
            </Row>
            <Converter
              value={amount}
              size="large"
              placeholderFrom="0.00"
              placeholderTo="0.00"
              prefixFrom={currencySymbols[accountFrom.currency]}
              prefixTo={currencySymbols[accountTo.currency]}
              onChange={this.handleInputChange}
              rate={rateInfo?.rate}
              errorMessage={errorMessage}
            />
            {accountFrom.currency === rateInfo?.currencyFrom && accountTo.currency === rateInfo?.currencyTo ? (
              <Text>
                The transfer will occur at the rate 1 {currencySymbols[accountFrom.currency]} = {rateInfo.rate.toLocaleString('en')} {currencySymbols[accountTo.currency]}
              </Text>
            ) : <div className={styles.skeleton} />}
            <div className={styles.submit}>
              <Button
                size="large"
                type="primary"
                loading={loading}
                onClick={this.handleSubmit}
              >
                Exchange
              </Button>
            </div>
          </Col>
        </Row>
      </Interval>
    );
  }
}

ExchangeForm.propTypes = {};

export default ExchangeForm;
