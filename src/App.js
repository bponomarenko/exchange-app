import React, { useState } from 'react';
import styles from './App.module.css';
import ExchangeForm from './ExchangeForm/ExchangeForm';

const makeRequest = (currencyFrom, currencyTo, amount) => {
  const [dateString] = new Date().toJSON().split('T');
  const url = 'https://currency-converter5.p.rapidapi.com/currency/historical/';

  return fetch(`
      ${url}${dateString}?format=json&from=${currencyFrom}&to=${currencyTo}&amount=${amount}
    `, {
    headers: {
      'x-rapidapi-host': 'currency-converter5.p.rapidapi.com',
      'x-rapidapi-key': '5e2b985617msh621493758104c8ep127a92jsnc20b467212eb'
    }
  })
    .then(res => res.json())
    .then(({ rates }) => {
      const { rate, rate_for_amount } = rates[currencyTo];

      return {
        currencyFrom,
        currencyTo,
        rate: Number(rate),
        amount: Number(rate_for_amount)
      };
    });
};

function App() {
  const [accounts, setAccounts] = useState(
    [
      { id: 'RevolutUSD', currency: 'USD', value: 1000 },
      { id: 'RevolutEUR', currency: 'EUR', value: 1000 },
      { id: 'RevolutGBP', currency: 'GBP', value: 1000 },
    ]
  );
  const emulateExchange = (amount, rate, accountFrom, accountTo) => {
    const round = value => Number(value.toFixed(2));
    const processedAccounts = accounts.map(({ id, currency, value }) => {
      if (id === accountFrom.id) {
        return {
          id,
          currency,
          value: round(value - amount)
        }
      }

      if (id === accountTo.id) {
        return {
          id,
          currency,
          value: round(value + amount * rate)
        }
      }

      return { id, currency, value }
    });

    return new Promise(resolve => setTimeout(() => {
      setAccounts(processedAccounts);
      resolve(processedAccounts);
    }, 1000));
  };

  return (
      <div className={styles.layout}>
        <ExchangeForm accounts={accounts} onExchange={emulateExchange} makeRequest={makeRequest} />
      </div>
  );
}

export default App;
