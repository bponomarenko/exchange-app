import { PureComponent } from 'react';
import PropTypes from 'prop-types';

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

export default class Interval extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    skipFirst: PropTypes.bool,
    delay: PropTypes.number,
    callback: PropTypes.func.isRequired
  };

  timerId;
  continues = true;

  componentDidMount() {
    const { delay, skipFirst } = this.props;

    if (skipFirst) {
      return this.timerId = setTimeout(this.execute, delay)
    }

    return this.execute();
  }

  componentWillUnmount() {
    clearTimeout(this.timerId);
    this.continues = false;
  }

  execute = () => requestAnimationFrame(() => {
    const { callback, delay } = this.props;

    if (!this.continues) {
      return;
    }

    return Promise.race([Promise.resolve(callback()), wait(delay)])
      .finally(() => this.timerId = setTimeout(this.execute, delay))
  });

  render() {
    return this.props.children;
  }
}
