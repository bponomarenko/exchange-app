import React from 'react';
import { shallow } from 'enzyme';
import Interval from '../Interval';

describe('Interval', () => {
  beforeAll(() => {
    jest.spyOn(window, 'requestAnimationFrame').mockImplementation((cb) => cb());
  });

  afterAll(() => {
    window.requestAnimationFrame.mockRestore();
  });

  it('Runs callback every second', async (done) => {
    const callback = jest.fn();
    shallow(<Interval callback={callback} delay={1000} />);

    expect(callback).toBeCalledTimes(1);
    setTimeout(() => {
      expect(callback).toBeCalledTimes(2);
    }, 1500);
    setTimeout(() => {
      expect(callback).toBeCalledTimes(3);
      done();
    }, 2500);
  });

  it(`Doesn't call callback when skipFirst prop specified`, () => {
    const callback = jest.fn();
    shallow(<Interval callback={callback} delay={1000} skipFirst />);

    expect(callback).toBeCalledTimes(0);
  });

  it(`Doesn't call callback if component unmounted`, (done) => {
    const callback = jest.fn();
    const component = shallow(<Interval callback={callback} delay={1000} />);

    expect(callback).toBeCalledTimes(1);
    component.unmount();
    setTimeout(() => {
      done();
      expect(callback).toBeCalledTimes(1);
    }, 3000);
  });

  it(`Call callback every second even if previous callback didn't resolve`, (done) => {
    const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
    const callback = jest.fn(() => wait(5000));
    shallow(<Interval callback={callback} delay={1000} />);

    expect(callback).toBeCalledTimes(1);
    setTimeout(() => {
      expect(callback).toBeCalledTimes(2);
      done();
    }, 2500);
  })
});
