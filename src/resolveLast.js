export default (fn) => {
  let pendingPromise;
  return (...args) => new Promise((resolve, reject) => {
      const promise = fn(...args)
        .then(payload => {
          if (promise === pendingPromise) {
            resolve(payload)
          }
        })
        .catch(error => {
          if (promise === pendingPromise) {
            reject(error)
          }
        });

      pendingPromise = promise;
  })
}
