import numberInputHOC from '../numberInputHOC';
import React from 'react';
import { Input } from 'antd';
import { mount } from 'enzyme';

const NumberInput = numberInputHOC(Input);

describe('numberInputHOC', () => {
  it('allow to enter only numbers and dot', () => {
    let value = 100;
    const component = mount(<NumberInput value={100} onChange={(v) => value = v} />);

    component.find('input').simulate('change', { target: { value: '300.45' } });

    expect(value).toBe(300.45);
    component.setProps({ value });

    component.find('input').simulate('change', { target: { value: 'foobar' } });
    expect(value).toBe(300.45);
  });

  it('convert value with comma to number with dot', () => {
    let value = 100;
    const component = mount(<NumberInput value={100} onChange={(v) => value = v} />);

    component.find('input').simulate('change', { target: { value: '333,33' } });

    expect(value).toBe(333.33);
    component.setProps({ value });
  });

  it('format value on blur', () => {
    const component = mount(<NumberInput value={1000000.99} onChange={() => {}} />);

    component.find('input').simulate('blur');

    expect(component.find('input').prop('value')).toBe('1,000,000.99')
  });

  it('show row value on focus', () => {
    const component = mount(<NumberInput value={1000000.99} onChange={() => {}} />);

    component.find('input').simulate('focus');

    expect(component.find('input').prop('value')).toBe('1000000.99')
  });

  it('return undefined on input cleaning', () => {
    let value = 100;
    const component = mount(<NumberInput value={100} onChange={(v) => value = v} />);

    component.find('input').simulate('change', { target: { value: '' } });

    expect(value).toBe(undefined);
  });
});
